package com.bbva.primerentregabletechu.Modelo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
@Document(collection = "cita")
public class Citas {
    @Id
    public String idCita;
    public String idPaciente;
    private String idMedico;
    private String fechaCita;
    private List<Medico> medico;

    public Citas() {

    }

    public Citas(String idCita, String idPaciente,String idMedico, String fechaCita,List<Medico> medico) {
        this.idCita = idCita;
        this.idPaciente = idPaciente;
        this.idMedico = idMedico;
        this.fechaCita = fechaCita;
        this.medico =medico;

    }

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }

    public String getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(String idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(String idMedico) {
        this.idMedico = idMedico;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public List<Medico> getMedico() {
        if(this.medico == null){
            this.medico = new ArrayList<>();
        }
        return medico;
    }

    public void setMedico(List<Medico> medico) {
        this.medico = medico;
    }
}
