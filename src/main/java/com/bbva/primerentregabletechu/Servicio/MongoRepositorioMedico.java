package com.bbva.primerentregabletechu.Servicio;

import com.bbva.primerentregabletechu.Modelo.Medico;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MongoRepositorioMedico extends MongoRepository<Medico, String> {
}
