package com.bbva.primerentregabletechu.Servicio;

import com.bbva.primerentregabletechu.Modelo.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import java.util.List;
import java.util.Optional;

@Service
public class ServicioPaciente {
    @Autowired
    MongoRepositorioPaciente mongoRepository;


    public Optional<Paciente> findById(String id) {
        return this.mongoRepository.findById(id);
    }

    public Paciente save(Paciente pa) {
        return this.mongoRepository.insert(pa);
    }

    public void guardarPacienteorId(String idCliente, Paciente paciente) {
        paciente.setIdPaciente(idCliente);
        this.mongoRepository.save(paciente);
    }

    public long count() {
        return mongoRepository.count();
    }

    public List<Paciente> findAll() {
        return mongoRepository.findAll();
    }

    public void guardarPacientePorId(String idPaciente, Paciente paciente) {
        paciente.setIdPaciente(idPaciente);
        this.mongoRepository.save(paciente);
    }

    public Paciente buscarPorId(String id) {
        Optional<Paciente> Paciente = this.mongoRepository.findById(id);
        return Paciente.isPresent() ? Paciente.get() : null;
    }

    @Autowired
    MongoOperations mongoOperations;

    public void eliminarPorIdPaciente(String Curp) {
        final Query query = new Query();
        query.addCriteria(Criteria.where("idPaciente").is(Curp));
        Paciente paciente = mongoOperations.findAndRemove(query, Paciente.class);
        System.out.println("Deleted document : " + paciente);
    }

}
