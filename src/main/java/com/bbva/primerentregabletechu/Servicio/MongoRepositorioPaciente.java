package com.bbva.primerentregabletechu.Servicio;

import com.bbva.primerentregabletechu.Modelo.Paciente;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoRepositorioPaciente extends MongoRepository<Paciente, String> {
}
