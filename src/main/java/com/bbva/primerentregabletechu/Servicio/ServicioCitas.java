package com.bbva.primerentregabletechu.Servicio;

import com.bbva.primerentregabletechu.Modelo.Citas;
import com.bbva.primerentregabletechu.Modelo.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ServicioCitas {

    @Autowired
    MongoRepositorioCitas mongoRepository;


    public Optional<Citas> findById(String id) {
        return this.mongoRepository.findById(id);
    }

    public Citas save(Citas ci) {
        return this.mongoRepository.insert(ci);
    }

    public void guardarCitasorId(String idCita, Citas cita) {
        cita.setIdCita(idCita);
        this.mongoRepository.save(cita);
    }

    public long count() {
        return mongoRepository.count();
    }

    public List<Citas> findAll() {
        return mongoRepository.findAll();
    }

    public void guardarCitaPorId(String idCita, Citas cita) {
        cita.setIdCita(idCita);
        this.mongoRepository.save(cita);
    }

    public Citas buscarPorId(String id) {
        Optional<Citas> citas = this.mongoRepository.findById(id);
        return citas.isPresent() ? citas.get() : null;
    }

    @Autowired
    MongoOperations mongoOperations;

    public void eliminarPorIdCita(String Curp) {
        final Query query = new Query();
        query.addCriteria(Criteria.where("idCita").is(Curp));
        Citas cita = mongoOperations.findAndRemove(query, Citas.class);
        System.out.println("Deleted document : " + cita);
    }

}
