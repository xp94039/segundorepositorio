package com.bbva.primerentregabletechu.Servicio;

import com.bbva.primerentregabletechu.Modelo.Medico;
import com.bbva.primerentregabletechu.Modelo.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class ServicioMedico {

    @Autowired
    MongoRepositorioMedico mongoRepository;


    public Optional<Medico> findById(String id) {
        return this.mongoRepository.findById(id);
    }

    public Medico save(Medico medico) {
        return this.mongoRepository.insert(medico);
    }

    public void guardarMedicoorId(String idMedico, Medico medico) {
        medico.setIdMedico(idMedico);
        this.mongoRepository.save(medico);
    }

    public long count() {
        return mongoRepository.count();
    }

    public List<Medico> findAll() {
        return mongoRepository.findAll();
    }

    public void guardarMedicoPorId(String idMedico, Medico medico) {
        medico.setIdMedico(idMedico);
        this.mongoRepository.save(medico);
    }

    public Medico buscarPorId(String id) {
        Optional<Medico> medico = this.mongoRepository.findById(id);
        return medico.isPresent() ? medico.get() : null;
    }

    @Autowired
    MongoOperations mongoOperations;

    public void eliminarPorIdMedico(String curp) {
        final Query query = new Query();
        query.addCriteria(Criteria.where("idMedico").is(curp));
        Medico medico = mongoOperations.findAndRemove(query, Medico.class);
        System.out.println("Deleted document : " + medico);
    }

}
