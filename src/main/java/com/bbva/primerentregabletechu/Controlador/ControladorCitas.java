package com.bbva.primerentregabletechu.Controlador;

import com.bbva.primerentregabletechu.Modelo.Citas;
import com.bbva.primerentregabletechu.Modelo.Medico;
import com.bbva.primerentregabletechu.Modelo.Paciente;
import com.bbva.primerentregabletechu.Servicio.ServicioCitas;
import com.bbva.primerentregabletechu.Servicio.ServicioMedico;
import com.bbva.primerentregabletechu.Servicio.ServicioPaciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@CrossOrigin
@RestController
@RequestMapping("${api.v1}")
public class ControladorCitas {


    private final AtomicLong secuenciaIds = new AtomicLong(0L);

    @Autowired
    ServicioPaciente servicioPaciente;
    @Autowired
    ServicioCitas servicioCitas;

    @Autowired
    ServicioMedico servicioMedico;


    // Get de todos las citas
    @GetMapping("/citas")
    public  List<Citas> getCitas() {
        return servicioCitas.findAll();
    }

   // Get de citas por id del paciente
    @GetMapping("/pacientes/{idPaciente}/citas")
    public ResponseEntity citasPorPaciente(@PathVariable String idPaciente) {
        Paciente pr = servicioPaciente.buscarPorId(idPaciente);

        if (pr == null) {
            return new ResponseEntity<>("Paciente no encontrado.", HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok(pr);

    }
    // POST para agregar citas del paciente
    @PostMapping("/citas")
    public ResponseEntity<String> agregarCitas(@RequestBody Citas cita) {
        Medico m = servicioMedico.buscarPorId(cita.getIdMedico());
        Paciente p = servicioPaciente.buscarPorId(cita.getIdPaciente());
        if(p == null && m == null){
            return new ResponseEntity<>("Paciente y Medico no registrado.", HttpStatus.NOT_FOUND);
        }else if(m == null){
            return new ResponseEntity<>("Medico no registrado.", HttpStatus.NOT_FOUND);
        } else if(p == null){
            return new ResponseEntity<>("Paciente no registrado.", HttpStatus.NOT_FOUND);
        } else {
            cita.getMedico().add(m);
            p.getCitas().add(cita);
           this.servicioCitas.save(cita);
           this.servicioPaciente.guardarPacienteorId(p.getIdPaciente(), p);
           return new ResponseEntity<>("Cita creada satisfactoriamente!", HttpStatus.CREATED);
        }
    }

    // PATCH para agregar citas de pacientes por ID
    @PatchMapping("/pacientes/{idPaciente}/citas")
    public ResponseEntity agregarCitasPaciente(@PathVariable String idPaciente, @RequestBody Citas cit) {
        Paciente pa = servicioPaciente.buscarPorId(idPaciente);
        Medico me = servicioMedico.buscarPorId(cit.getIdMedico());
        if (pa != null && me != null) {

                pa.getCitas().add(cit);
                cit.getMedico().add(me);
                this.servicioCitas.save(cit);

        } else {
            return new ResponseEntity<>("Paciente o Medico no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(cit);
    }

    // DELETE citas
    @DeleteMapping("/citas/{id}")
    public ResponseEntity deleteCitas(@PathVariable String id) {
        Citas pr = servicioCitas.buscarPorId(id);
        if(pr == null) {
            return new ResponseEntity<>("Cita no encontrada.", HttpStatus.NOT_FOUND);
        }
        this.servicioCitas.eliminarPorIdCita(id);
        return new ResponseEntity<>("Cita eliminado correctamente.", HttpStatus.OK); // También 204 No Content
    }

    // PUT modificar citas por Id de cita
    @PatchMapping("/citas/{idCitas}")
    public ResponseEntity updateCitas(@PathVariable(name = "idCitas") String id,
                                       @RequestBody Citas p) {
        try {
            Citas pr = servicioCitas.buscarPorId(id);
            Medico me = servicioMedico.buscarPorId(p.getIdMedico());
            Paciente pa = servicioPaciente.buscarPorId(p.getIdPaciente());

           if(pr == null){
                return new ResponseEntity<>("Cita no encontrada.", HttpStatus.NOT_FOUND);
            } else if(me == null){
                return new ResponseEntity<>("Medico no encontrado.", HttpStatus.NOT_FOUND);
            } else if(pa == null ){
               return new ResponseEntity<>("Paciente no encontrado.", HttpStatus.NOT_FOUND);
           } else {
             /*  pr.setIdPaciente(p.getIdPaciente());
               pr.setIdMedico(p.getIdMedico());*/
               pr.getMedico().add(me);
             //  pr.setFechaCita(p.getFechaCita());
               this.servicioCitas.guardarCitaPorId(id, pr);
               return new ResponseEntity<>("Cita actualizada correctamente.", HttpStatus.OK);
            }

        }catch (Exception x){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }
}
