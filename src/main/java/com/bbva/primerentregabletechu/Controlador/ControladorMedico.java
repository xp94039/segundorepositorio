package com.bbva.primerentregabletechu.Controlador;

import com.bbva.primerentregabletechu.Modelo.Citas;
import com.bbva.primerentregabletechu.Modelo.Paciente;
import com.bbva.primerentregabletechu.Servicio.ServicioCitas;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bbva.primerentregabletechu.Modelo.Medico;
import com.bbva.primerentregabletechu.Servicio.ServicioMedico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("${api.v1}")
public class ControladorMedico {

    @Autowired
    ServicioMedico servicioMedico;
    @Autowired
    ServicioCitas servicioCitas;

    @GetMapping("")
    public String root() {
        return "Techu API REST v1.0.0";
    }

    // Get de todos los Pacientes
    @GetMapping("/medicos")
    public  List<Medico> getMedicos() {
        return servicioMedico.findAll();
    }

    // GET instancia por id
    @GetMapping("/medicos/{id}")
    public ResponseEntity getMedicoId(@PathVariable String id) {
        Medico pr = servicioMedico.buscarPorId(id);
        if (pr == null) {
            return new ResponseEntity<>("medico no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    // insertar
    @PostMapping("/medicos")
    public Medico postMedicos(@RequestBody Medico medico){
        servicioMedico.save(medico);
        return medico;
    }
    // PUT
    @PutMapping("/medicos/{idMedico}")
    public ResponseEntity updateMedico(@PathVariable(name = "idMedico") String id,
                                         @RequestBody Medico p) {
      try {
          Medico pr = servicioMedico.buscarPorId(id);
          if (pr == null) {
              return new ResponseEntity<>("Medico no encontrado.", HttpStatus.NOT_FOUND);
          }
          pr.setNombre(p.getNombre());
          pr.setApellidos(p.getApellidos());
          pr.setEspecialidad(p.getEspecialidad());
          pr.setPrecioConsulta(p.getPrecioConsulta());
          this.servicioMedico.guardarMedicoPorId(id, pr);

          return new ResponseEntity<>("Medico actualizado correctamente.", HttpStatus.OK);

      }catch (Exception x){
          throw new ResponseStatusException(HttpStatus.FORBIDDEN);
      }
    }

    // DELETE
    @DeleteMapping("/medicos/{id}")
    public ResponseEntity deleteMedico(@PathVariable String id) {
        Medico pr = servicioMedico.buscarPorId(id);
        Citas cita= servicioCitas.buscarPorId(id);
        if(pr == null) {
            return new ResponseEntity<>("Medico no encontrado.", HttpStatus.NOT_FOUND);
        }else if( cita != null && cita.getIdMedico() == id){
            return new ResponseEntity<>("Medico ya tiene asignado una cita, primero elimine cita.", HttpStatus.CONFLICT);
        } else {
            servicioMedico.eliminarPorIdMedico(id);
            return new ResponseEntity<>("Medico eliminado correctamente.", HttpStatus.OK); // También 204 No Content

        }

    }

}
