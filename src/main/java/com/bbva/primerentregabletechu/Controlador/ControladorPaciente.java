package com.bbva.primerentregabletechu.Controlador;

import com.bbva.primerentregabletechu.Modelo.Citas;
import com.bbva.primerentregabletechu.Modelo.Medico;
import com.bbva.primerentregabletechu.Modelo.Paciente;
import com.bbva.primerentregabletechu.Servicio.ServicioCitas;
import com.bbva.primerentregabletechu.Servicio.ServicioMedico;
import com.bbva.primerentregabletechu.Servicio.ServicioPaciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@CrossOrigin
@RestController
@RequestMapping("${api.v1}")
public class ControladorPaciente {
    private final AtomicLong secuenciaIds = new AtomicLong(0L);

    @Autowired
    ServicioPaciente servicioPaciente;

    @Autowired
    ServicioCitas servicioCitas;


// listartodo

    @GetMapping("/pacientes")
    public List<Paciente> getPacientes() {
        return servicioPaciente.findAll();
    }


// listar por id

    @GetMapping("/pacientes/{id}" )
    public Optional<Paciente> getPacienteId(@PathVariable String id){
        Optional<Paciente> paciente = this.servicioPaciente.findById(id);
        if (paciente.isPresent()) {
            return servicioPaciente.findById(id);
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Paciente no encontrado");

    }

// insertar

    @PostMapping("/pacientes")
    public ResponseEntity<Paciente> agregarClientes(@RequestBody Paciente cli) {
            Paciente paciente = this.servicioPaciente.save(cli);
            if (paciente == null) {
                throw new ResponseStatusException(HttpStatus.NOT_MODIFIED, "No se agrego el paciente");
            }
            return ResponseEntity.ok(paciente);

    }

    // PUT
    @PutMapping("/pacientes/{idPaciente}")
    public ResponseEntity updatePaciente(@PathVariable(name = "idPaciente") String id,
                                         @RequestBody Paciente p) {
        Paciente pr = servicioPaciente.buscarPorId(id);
        if (pr == null) {
            return new ResponseEntity<>("Paciente no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setNombre(p.getNombre());
        pr.setApellidos(p.getApellidos());
        pr.setEdad(p.getEdad());
        pr.setAltura(p.getAltura());
        pr.setDni(p.getDni());
        pr.setGenero(p.getGenero());
        pr.setPeso(p.getPeso());
        pr.setFormaDePago(p.getFormaDePago());
        this.servicioPaciente.guardarPacienteorId(id, pr);

        return new ResponseEntity<>("Paciente actualizado correctamente.", HttpStatus.OK);
    }

    // DELETE
    @DeleteMapping("/pacientes/{id}")
    public ResponseEntity deletePaciente(@PathVariable String id) {
        Paciente pr = servicioPaciente.buscarPorId(id);
        Citas cita= servicioCitas.buscarPorId(id);

        if(pr == null) {
            return new ResponseEntity<>("Paciente no encontrado.", HttpStatus.NOT_FOUND);
        }else if( cita != null && Integer.parseInt(cita.getIdPaciente())== Integer.parseInt(id)){
            return new ResponseEntity<>("Paciente ya tiene registrado una cita, primero elimine cita.", HttpStatus.CONFLICT);
        } else{
            this.servicioPaciente.eliminarPorIdPaciente(id);
            return new ResponseEntity<>("Paciente eliminado correctamente.", HttpStatus.OK); // También 204 No Content

        }

    }

}
